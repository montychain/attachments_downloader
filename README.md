## Загрузка вложений писем

### Установка
* Скачайте этот репозиторий по ссылке: [https://bitbucket.org/montychain/attachments_downloader/downloads/](https://bitbucket.org/montychain/attachments_downloader/downloads/)
* Распакуйте его в произвольную папку
* Кликните правой кнопкой мыши по этой папке и нажмите **New Terminal at Folder**
* В терминале запустите build.sh командой
```bash
source build.sh
```
* Откройте **auth.txt** в любом текстовом редакторе
* Замените значение в первой строчке **your-address@gmail.com** на адрес вашего email в GMAIL.COM
* Замените значение во второй строчке **your-password** на ваш пароль в GMAIL.COM
* Сохраните изменения **auth.txt** и закройте текстовый редактор


#### Включите IMAP в настройках GMAIL.COM
* Зайдите в настройки GMAIL.COM 
* В настройках зайдите во вкладку **"Пересылка и POP/IMAP"**.
* Нажмите опцию **"Включить IMAP"**
* Нажмите внизу кнопку **"Сохранить изменения"**


#### Авторизация программы в GMAIL.COM
Чтобы программа **a_dl** могла авторизоваться в gmail.com необходимо дать
ей такое разрешение. Для этого в браузере откройте ссылку:
```
https://myaccount.google.com/lesssecureapps?pli=1
```
Нужно переключить опцию **"Небезопасные приложения заблокированы"** на 
**"Небезопасные приложения разрешены"**
 

#### Фильтрация вложений от избранных отправителей
Если файл **whitelist.txt** пустой либо отсутсвует, программа **a_dl** будет скачивать все вложения в почтовом ящике.  
Однако, есть возможность скачивать вложения только от избранных отправителей.  
Для этого:
* Откройте в любом текстовом редакторе файл **whitelist.txt**
* Добавьте список почтовых адресов отправителей, чьи вложения требуется скачать (по одному адресу на строчку), например:
```
example-1@gmail.com
example-2@yandex.ru
example-3@mail.ru
```
_Примечание: файл **whitelist.txt** должен лежать в той же папке, что и **a_dl**_



### Запуск программы a_dl
_Тестировалось на англоязычном MAC_

* Зайдите в папку репозитория, которую вы только что распаковали из архива
* Кликните дважды по программе **a_dl** (не путать с **a_dl.py**)
* Программа откроется в терминале и начнёт работу
* Каждые 2 минуты программа будет заходить снова и проверять не появилось ли новых вложений. Если таковые появились - она их скачает.    
Все вложения будут складываться в папку **attachments** внутри папки **montychain-attachments-downloader-..**.



### Добавление в a_dl автозапуск
* Кликните на иконку "яблока" в левом верхнем углу экрана
* Выберите пункт **System Preferences...**
* В открывшемся окне кликните по **Users and Groups**
* Выберите вкладку **Login Items**
* Внизу кликните по иконке **+** (плюсик) и найдите в файндере программу "a_dl" в папке репозитория, который вы недавно распаковывали
* Теперь вы можете закрыть окно настроек.   
Программа a_dl будет автоматически запускаться в терминале каждый раз когда вы зайдете в свою учётную запись на Mac


### Изменение минимальной даты отправки письма
Сейчас минимальная дата, с которй скачиваются письма - 10 Апреля 2020г.  
Эту дату можно поменять в файле **date.txt**  
Важно соблюдать формат даты в виде
```
10-Apr-2020
```
