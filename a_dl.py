import traceback
import platform
import hashlib
import imaplib
import email
import time
import sys
import os
import re
from email.header import decode_header
from collections import defaultdict, Counter


if getattr(sys, 'frozen', False):
    root_dir = os.path.dirname(sys.executable)
else:
    root_dir = os.path.dirname(os.path.realpath(__file__))
print("Root dir:", root_dir)
auth_file = os.path.join(root_dir, "auth.txt")
min_date_file = os.path.join(root_dir, "date.txt")
ATTACHMENTS_DIR = os.path.join(root_dir, "attachments")
RESUME_FILE = os.path.join(root_dir, "resume.txt")
WHITELIST_FILE = os.path.join(root_dir, "whitelist.txt")
white_list = set()
file_name_counter = Counter()
file_name_hashes = defaultdict(set)
new_message_ids = set()
processed_message_ids = set()
config = {
    "login": "",
    "password": "",
    "date": ""
}


def load_white_list():
    if not os.path.exists(WHITELIST_FILE):
        with open(WHITELIST_FILE, "w") as f:
            f.write("")
    with open(WHITELIST_FILE, "r") as f:
        for line in f.readlines():
            white_list.add(line.strip())
    print("White-listed email addresses:", white_list)


def recover():
    if os.path.exists(RESUME_FILE):
        print('Recovery file found resuming...')
        with open(RESUME_FILE) as f:
            processed_ids = f.read()
            for ProcessedId in processed_ids.split(','):
                processed_message_ids.add(ProcessedId)
    else:
        print('No Recovery file found.')
        open(RESUME_FILE, 'a').close()


def get_mail_box(imap_session):
    possible_names = ['"[Gmail]/&BBIEQQRP- &BD8EPgRHBEIEMA-"', '"[Gmail]/All Mail"']
    for name in possible_names:
        exists, data = imap_session.select(name)
        if exists == "OK":
            return True, imap_session
    return False, imap_session


def generate_mail_messages():
    imap_session = imaplib.IMAP4_SSL('imap.gmail.com')
    typ, account_details = imap_session.login(config["login"], config["password"])
    print(typ)
    print(account_details)
    if typ != 'OK':
        print('Not able to sign in!')
        raise NameError('Not able to sign in!')
    success, imap_session = get_mail_box(imap_session)
    if not success:
        print("Could not find All Mail folder!")
        raise NameError("Could not find All Mail folder!")
    search_params = '(X-GM-RAW "has:attachment" SINCE "{}")'.format(config["date"])
    typ, data = imap_session.search(None, search_params)
    if typ != 'OK':
        print('Error searching Inbox.')
        raise NameError('Error searching Inbox.')

    # Iterating over all emails
    for message_id in data[0].split():
        new_message_ids.add(message_id)
        typ, message_parts = imap_session.fetch(message_id, '(RFC822)')
        if typ != 'OK':
            print('Error fetching mail.')
            raise NameError('Error fetching mail.')
        email_body = message_parts[0][1]
        if message_id not in processed_message_ids:
            yield message_id, email.message_from_bytes(email_body)
            processed_message_ids.add(message_id)
            with open(RESUME_FILE, "a") as resume:
                resume.write('{id},'.format(id=message_id))
    imap_session.close()
    imap_session.logout()


def decoder(text):
    if '?UTF-8?' in text:
        return decode_header(text)[0][0].decode('utf-8')
    if '?koi8-r?' in text:
        return decode_header(text)[0][0].decode('koi8-r')
    return text


def join_headers(h):
    final_string_list = list()
    for data, encoding in decode_header(h):
        if encoding:
            final_string_list.append(data.decode(encoding))
        elif type(data) == bytes:
            final_string_list.append(data.decode('utf-8'))
        else:
            final_string_list.append(data)
    return " # ".join(final_string_list)


def save_attachments(message_id, message, directory):
    message_id = "[" + message_id.decode('utf-8') + "]"
    sender = join_headers(message['from'])
    sender = re.sub(r'[\\/*?:"<>|]', "", sender)
    sender_dir = os.path.join(ATTACHMENTS_DIR, sender)
    if len(white_list) > 0 and not any(wl_address.lower() in sender.lower() for wl_address in white_list):
        return
    for part in message.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue
        file_name = part.get_filename()
        if file_name is not None:
            file_name = decoder(''.join(file_name.splitlines()))
        if file_name:
            file_str, file_extension = os.path.splitext(file_name)
            file_name = file_str + " " + message_id + file_extension
            if not os.path.exists(sender_dir):
                os.mkdir(sender_dir)
            payload = part.get_payload(decode=True)
            if payload:
                x_hash = hashlib.md5(payload).hexdigest()
                if x_hash in file_name_hashes[file_name]:
                    print('\tSkipping duplicate file: {file}'.format(file=file_name))
                    continue
                file_name_counter[sender + "-" + file_name] += 1
                file_str, file_extension = os.path.splitext(file_name)
                if file_name_counter[file_name] > 1:
                    new_file_name = '{file}({suffix}){ext}'\
                        .format(suffix=file_name_counter[file_name], file=file_str, ext=file_extension)
                    print('\tRenaming and storing: {file} to {new_file}'
                          .format(file=file_name, new_file=new_file_name))
                else:
                    new_file_name = file_name
                    print('\tStoring: {file}'.format(file=file_name))
                file_name_hashes[file_name].add(x_hash)
                file_path = os.path.join(directory, sender, new_file_name)
                if os.path.exists(file_path):
                    print('\tExists in destination: {file}'.format(file=new_file_name))
                    continue
                try:
                    with open(file_path, 'wb') as fp:
                        fp.write(payload)
                        print("Saved file at:", file_path)
                except EnvironmentError:
                    print('Could not store: {file} it has invalid file name or path under {op_sys}.'.format(
                        file=file_path,
                        op_sys=platform.system()))
            else:
                print('Attachment {file} was returned as type: {f_type} skipping...'.format(file=file_name,
                                                                                            f_type=type(payload)))
                continue


def get_config():
    with open(auth_file, "r") as f:
        lines = [line.strip() for line in f.readlines()]
    config["login"] = lines[0]
    config["password"] = lines[1]
    assert config["login"] != "your-address@gmail.com", "Please add your real Gmail address and password to auth.txt!"
    with open(min_date_file, "r") as f:
        config["date"] = f.read().strip()


def iteration():
    get_config()
    load_white_list()
    recover()
    if not os.path.exists(ATTACHMENTS_DIR):
        os.mkdir(ATTACHMENTS_DIR)
    for m_id, msg in generate_mail_messages():
        save_attachments(m_id, msg, ATTACHMENTS_DIR)
    os.remove(RESUME_FILE)


def do_work():
    get_config()
    while True:
        try:
            iteration()
        except Exception as exc:
            print("Iteration failed:", traceback.format_exc(exc))
        print("Sleeping for 2 minutes before the next iteration.........")
        time.sleep(120)


if __name__ == '__main__':
    do_work()
